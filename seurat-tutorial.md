---
title: Single-cell RNAseq Primer with Seurat
author: Tyler Schappe
date: 6/7/2022
output: 
  html_document:
    toc: yes
    toc_float: 
      collapsed: TRUE
      smooth_scroll: TRUE
    toc_depth: '6'
    number_sections: TRUE
    keep_md: TRUE
---

References:

- There is an extensive [tutorial](https://scrnaseq-course.cog.sanger.ac.uk/website/index.html) from a course taught at the University of Cambridge.
- This notebook is adapted from the [Seurat tutorial](https://satijalab.org/seurat/articles/pbmc3k_tutorial.html)

# Setup

## Load Libraries


```r
library(tidyverse)
```

```
## Warning in system("timedatectl", intern = TRUE): running command 'timedatectl'
## had status 1
```

```
## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.1 ──
```

```
## ✔ ggplot2 3.3.6     ✔ purrr   0.3.4
## ✔ tibble  3.1.7     ✔ dplyr   1.0.9
## ✔ tidyr   1.2.0     ✔ stringr 1.4.0
## ✔ readr   2.1.2     ✔ forcats 0.5.1
```

```
## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()
```

```r
library(Seurat)
```

```
## Attaching SeuratObject
```

```
## Attaching sp
```

```r
library(patchwork)
library(dplyr)
library(reticulate)
```

## Load and Prepare Data

Here we assume that the 10X CellRanger pipeline has been run to generate the count of matrices.


```r
source("config.R")
```

```
## here() starts at /hpc/home/ts415/project_repos/seurat-scrnaseq-primer
```

```r
#Set DATA directory variable
Sys.setenv(DATA=scrnaseq_data_dir)
```

Inspect the data files downloaded. The files of interest are in `data/filtered_gene_bc_matrices/hg19` and consist of

- `barcodes.tsv` A list of the 2,700 barcodes used, where each barcode represents a cell
- `genes.tsv` A mapping of gene IDs to gene symbols    
- `matrix.mtx` A matrix containing the UMI count per cell and gene combination



```r
print(system("head -3 $SCRNASEQ_DATA_DIR/filtered_gene_bc_matrices/hg19/barcodes.tsv", 
       intern = T))
```

```
## [1] "AAACATACAACCAC-1" "AAACATTGAGCTAC-1" "AAACATTGATCAGC-1"
```

```r
print(system("cat $SCRNASEQ_DATA_DIR/filtered_gene_bc_matrices/hg19/barcodes.tsv | wc -l", intern = T))
```

```
## [1] "2700"
```


```r
print(system("head -3 $DATA/filtered_gene_bc_matrices/hg19/genes.tsv", 
       intern = T))
```

```
## [1] "ENSG00000243485\tMIR1302-10" "ENSG00000237613\tFAM138A"   
## [3] "ENSG00000186092\tOR4F5"
```

```r
print(system("cat $DATA/filtered_gene_bc_matrices/hg19/genes.tsv | wc -l", intern = T))
```

```
## [1] "32738"
```

The first line just reports the number of lines in `genes.tsv`, `barcodes.tsv` and `matrix.mtx` respectively. Subsequent lines are read as follows

- First column is gene_id given as line number in `genes.tsv`
- Second column is cell_id given as line number in `barcodes.tsv`
- Third column is UMI count for that cell/gene combination

Note that this particular organization is space-saving for sparse matrices. If we created a regular matrix of cells $\times$ genes, most entries would be filled by zeros.


```r
# print(system("head -10 $DATA/filtered_gene_bc_matrices/hg19/matrix.mtx"
             # , intern = T))

head(read.table(file.path(scrnaseq_data_dir, "filtered_gene_bc_matrices", "hg19", "matrix.mtx"), skip = 2), n = 5)
```

```
##      V1   V2      V3
## 1 32738 2700 2286884
## 2 32709    1       4
## 3 32707    1       1
## 4 32706    1      10
## 5 32704    1       1
```

```r
tail(read.table(file.path(scrnaseq_data_dir, "filtered_gene_bc_matrices", "hg19", "matrix.mtx"), skip = 2), n = 5)
```

```
##          V1   V2 V3
## 2286881 179 2700  2
## 2286882 167 2700  4
## 2286883 122 2700  1
## 2286884 121 2700  1
## 2286885  82 2700  1
```

## Create a Seurat Object & Base Filters

Filtering includes:
- Min of 3 cells for each gene
  + Implies we will not be able to find cell subpopulations that consist of < 3 cells
- Min of 200 genes for each cell


```r
pbmc.data <- Read10X(data.dir = paste(scrnaseq_data_dir, "filtered_gene_bc_matrices/hg19/", sep="/"))
pbmc <- CreateSeuratObject(
    counts = pbmc.data, 
    project = "pbmc3k", 
    min.cells = 3, 
    min.features = 200)
```

```
## Warning: Feature names cannot have underscores ('_'), replacing with dashes
## ('-')
```

```r
pbmc
```

```
## An object of class Seurat 
## 13714 features across 2700 samples within 1 assay 
## Active assay: RNA (13714 features, 0 variable features)
```

Take a look at the first 3 genes and 30 cells

```r
pbmc.data[c("CD3D", "TCL1A", "MS4A1"), 1:30]
```

```
## 3 x 30 sparse Matrix of class "dgCMatrix"
```

```
##    [[ suppressing 30 column names 'AAACATACAACCAC-1', 'AAACATTGAGCTAC-1', 'AAACATTGATCAGC-1' ... ]]
```

```
##                                                                    
## CD3D  4 . 10 . . 1 2 3 1 . . 2 7 1 . . 1 3 . 2  3 . . . . . 3 4 1 5
## TCL1A . .  . . . . . . 1 . . . . . . . . . . .  . 1 . . . . . . . .
## MS4A1 . 6  . . . . . . 1 1 1 . . . . . . . . . 36 1 2 . . 2 . . . .
```

# Quality Control

## Calculate QC Metrics

Damaged or dead cells lose cytoplasmic RNA more easily than mitochondrial RNA, so a high proportion of mitochondrial genes is an indicator of a poor quality cell.

Calculate percent mitochondrial

```r
pbmc[["percent.mt"]] <- PercentageFeatureSet(pbmc, pattern = "^MT-")
```

Take a look at the metadata

```r
head(pbmc@meta.data, 5)
```

```
##                  orig.ident nCount_RNA nFeature_RNA percent.mt
## AAACATACAACCAC-1     pbmc3k       2419          779  3.0177759
## AAACATTGAGCTAC-1     pbmc3k       4903         1352  3.7935958
## AAACATTGATCAGC-1     pbmc3k       3147         1129  0.8897363
## AAACCGTGCTTCCG-1     pbmc3k       2639          960  1.7430845
## AAACCGTGTATGCG-1     pbmc3k        980          521  1.2244898
```

## Visualize QC Metrics


```r
VlnPlot(pbmc, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-10-1.png)<!-- -->


```r
plot1 <- FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "percent.mt")
plot2 <- FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
plot1 + plot2
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-11-1.png)<!-- -->

## Filter Cells by QC Metrics

### Set Desired Quantiles

These are the percentiles of observed values that we want to keep from our sample. For example, we will keep all cells that fall within the 1% to 99% range of features per cell.

- Features per cell: 1% - 99%
- UMI counts per cell: <= 95%
- Percent mitochondrial features: <= 95%


```r
features.quant <- c(0.01, 0.99)
counts.quant <- 0.95
mt.filter.quant <- 0.95
```

### Calculate the Quantile Values


```r
nFeature_RNA.quantiles <- quantile(pbmc$nFeature_RNA, features.quant)
nCount_RNA.quantiles <- quantile(pbmc$nCount_RNA, counts.quant)
percent.mt.quantiles <- quantile(pbmc$percent.mt, mt.filter.quant)
```

Examine the features per cell calculated quantiles

```r
nFeature_RNA.quantiles
```

```
##      1%     99% 
##  325.00 1739.02
```

```r
nCount_RNA.quantiles
```

```
##    95% 
## 4217.6
```

```r
percent.mt.quantiles
```

```
##      95% 
## 4.014896
```

### Filter the Cells


```r
pbmc.filt <- subset(
    pbmc, 
    subset = 
            #Features per cell
            nFeature_RNA >= nFeature_RNA.quantiles[1] &
            nFeature_RNA <= nFeature_RNA.quantiles[2] &
      
            #Counts per cell
            nCount_RNA <= nCount_RNA.quantiles &
        
            #Percent mitochondrial features
            percent.mt <= percent.mt.quantiles
)
```

### Calculate Percent Filtered


```r
data.frame(
    unfiltered.cell.count = ncol(pbmc@assays$RNA@counts),
    kept.cell.count = ncol(pbmc.filt@assays$RNA@counts),
    removed.cell.count = ncol(pbmc@assays$RNA@counts) - ncol(pbmc.filt@assays$RNA@counts),
    percent.retained = round(ncol(pbmc.filt@assays$RNA@counts) / ncol(pbmc@assays$RNA@counts) * 100, 2)
)
```

```
##   unfiltered.cell.count kept.cell.count removed.cell.count percent.retained
## 1                  2700            2417                283            89.52
```

Note that the percent retained is lower than the quantile range of any single filter we applied -- this is because we applied them jointly.

### Re-examine the QC Metrics


```r
VlnPlot(pbmc.filt, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-17-1.png)<!-- -->



```r
plot1 <- FeatureScatter(pbmc.filt, feature1 = "nCount_RNA", feature2 = "percent.mt")
plot2 <- FeatureScatter(pbmc.filt, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
plot1 + plot2
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-18-1.png)<!-- -->



# Normalization and Data Correction

## Calculate Cell Cycle Scores

### Normalize Data without Data Correction


```r
pbmc.filt <- SCTransform(
  pbmc.filt,
  vst.flavor = "v2",
  assay = 'RNA',
  new.assay.name = 'SCT',
  verbose = F
)
```

### Calculate Cell Cycle Scores

**Note:** Specifcy assay as normalized 'SCT' data created above


```r
#Built-in cell-cycle marker genes from Tirosh et al 2016
s.genes <- cc.genes$s.genes
g2m.genes <- cc.genes$g2m.genes

pbmc.filt <- CellCycleScoring(
  pbmc.filt,
  vst.flavor = "v2",
  s.features = s.genes,
  g2m.features = g2m.genes,
  assay = 'SCT',
  set.ident = TRUE
)
```

```
## Warning: The following features are not present in the object: DTL, UHRF1,
## MLF1IP, CCNE2, CDC45, CDC6, EXO1, DSCC1, CASP8AP2, CLSPN, POLA1, BRIP1, E2F8,
## not searching for symbol synonyms
```

```
## Warning: The following features are not present in the object: CDK1, BIRC5,
## TPX2, TOP2A, NUF2, MKI67, CENPF, FAM64A, CCNB2, CKAP2L, BUB1, GTSE1, HJURP,
## CDCA3, CDC20, TTK, CDC25C, KIF2C, DLGAP5, CDCA2, CDCA8, ECT2, HMMR, AURKA, ANLN,
## CENPE, NEK2, GAS2L3, CENPA, not searching for symbol synonyms
```

### Normalize and Correct for Cell Cycles

**Important:** Use original non-normalized counts data in @RNA slot


```r
pbmc.filt <- SCTransform(
  pbmc.filt,
  vst.flavor = "v2",
  assay = 'RNA',
  new.assay.name = 'SCT',
  vars.to.regress = c('S.Score', 'G2M.Score'),
  verbose = F
)
```


# Linear Dimension Reduction: PCA

Many genes in co-regulated pathways are likely to vary in the same way, and hence provide redundant information. PCA is a method to reduce linear redundancy and is often used as a first step in dimension reduction.

## Perform PCA


```r
pbmc.filt <- RunPCA(pbmc.filt, npcs = 30, verbose = FALSE)
```

Look at first 5 principle components and top loadings for each

```r
print(pbmc.filt[["pca"]], dims = 1:5, nfeatures = 5)
```

```
## PC_ 1 
## Positive:  FTL, LYZ, FTH1, CST3, S100A9 
## Negative:  MALAT1, RPS27A, RPS27, RPS6, LTB 
## PC_ 2 
## Positive:  NKG7, CCL5, GZMB, GNLY, GZMA 
## Negative:  HLA-DRA, CD74, CD79A, HLA-DPB1, HLA-DQA1 
## PC_ 3 
## Positive:  S100A8, S100A9, LYZ, RPS12, JUNB 
## Negative:  CD74, HLA-DRA, CD79A, HLA-DPB1, CD79B 
## PC_ 4 
## Positive:  FCGR3A, LST1, AIF1, IFITM3, IFITM2 
## Negative:  S100A8, S100A9, LYZ, LGALS2, GPX1 
## PC_ 5 
## Positive:  CCL5, GZMK, IL32, CD8A, B2M 
## Negative:  GNLY, GZMB, PRF1, FGFBP2, SPON2
```

## Visualize First 2 PC Axes


```r
DimPlot(pbmc.filt, reduction = "pca")
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-24-1.png)<!-- -->

## Visualize PC Loadings

Indicate how much variation each gene contributed to the axis


```r
VizDimLoadings(pbmc.filt, dims = 1:2, reduction = "pca")
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-25-1.png)<!-- -->

## Estimate Dimensionality of the Data Using an Elbow (Scree) Plot


```r
ElbowPlot(pbmc.filt)
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-26-1.png)<!-- -->

**Conclusion:** First 9 axes explain large majority of variation, then each axis after has diminishing returns.

# Cell Clustering

## Generate K-nearest Neighbor Graph

**Note:** Use only first 9 PC axes as determined above


```r
pbmc.filt <- FindNeighbors(pbmc.filt, reduction = "pca", dims = 1:15)
```

```
## Computing nearest neighbor graph
```

```
## Computing SNN
```

### Find Neighborhoods Using Leiden Algorithm

**Note:** Here we use resolution of 0.4

**Note:** For now, because of Python difficulties, we're using the SNN clustering algorithm instead of Leiden


```r
pbmc.filt <- FindClusters(pbmc.filt, algorithm = 3, resolution = 0.4, verbose = T)
```

```
## Modularity Optimizer version 1.3.0 by Ludo Waltman and Nees Jan van Eck
## 
## Number of nodes: 2417
## Number of edges: 88365
## 
## Running smart local moving algorithm...
## Maximum modularity in 10 random starts: 0.8970
## Number of communities: 8
## Elapsed time: 0 seconds
```

# Non-linear Dimension Reduction (UMAP)


```r
pbmc.filt <- RunUMAP(pbmc.filt, dims = 1:9, verbose = T)
```

```
## Warning: The default method for RunUMAP has changed from calling Python UMAP via reticulate to the R-native UWOT using the cosine metric
## To use Python UMAP via reticulate, set umap.method to 'umap-learn' and metric to 'correlation'
## This message will be shown once per session
```

```
## 11:04:22 UMAP embedding parameters a = 0.9922 b = 1.112
```

```
## 11:04:22 Read 2417 rows and found 9 numeric columns
```

```
## 11:04:22 Using Annoy for neighbor search, n_neighbors = 30
```

```
## 11:04:22 Building Annoy index with metric = cosine, n_trees = 50
```

```
## 0%   10   20   30   40   50   60   70   80   90   100%
```

```
## [----|----|----|----|----|----|----|----|----|----|
```

```
## **************************************************|
## 11:04:22 Writing NN index file to temp file /tmp/RtmpaPOz7L/file37829542e798ac
## 11:04:22 Searching Annoy index using 1 thread, search_k = 3000
## 11:04:23 Annoy recall = 100%
## 11:04:23 Commencing smooth kNN distance calibration using 1 thread
## 11:04:25 Initializing from normalized Laplacian + noise
## 11:04:25 Commencing optimization for 500 epochs, with 95836 positive edges
## 11:04:28 Optimization finished
```

## Visualize UMAP


```r
DimPlot(pbmc.filt, reduction = "umap")
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-30-1.png)<!-- -->

# Annotation

# Differential Gene Expression

## Calculate Corrected Counts 

This sets the sequencing depth to the median across all cells.


```r
pbmc.filt <- PrepSCTFindMarkers(pbmc.filt)
```

```
## Only one SCT model is stored - skipping recalculating corrected counts
```

## Compare Original to Corrected Counts


```r
pbmc.filt@assays$RNA@counts[c("CD3D", "TCL1A", "MS4A1"), 1:50]
```

```
## 3 x 50 sparse Matrix of class "dgCMatrix"
```

```
##    [[ suppressing 50 column names 'AAACATACAACCAC-1', 'AAACATTGATCAGC-1', 'AAACCGTGCTTCCG-1' ... ]]
```

```
##                                                                               
## CD3D  4 10 . . 1 2 3 1 . . 2 7 1 . . 2 . . . . . 3 4 5 1 2 . . 2 . 1 . . 6 . 2
## TCL1A .  . . . . . . 1 . . . . . . . . 1 . . . . . . . . . 1 1 . . . . . . 1 .
## MS4A1 .  . . . . . . 1 1 1 . . . . . . 1 2 . . 2 . . . . . 2 . . . . . . . . .
##                                  
## CD3D  3 3 . 2 5 2 . 6 . . . 4 2 .
## TCL1A . . . . . . . . . . . . . 5
## MS4A1 . . . . . . . . . . . . . 4
```

```r
pbmc.filt@assays$SCT@counts[c("CD3D", "TCL1A", "MS4A1"), 1:50]
```

```
## 3 x 50 sparse Matrix of class "dgCMatrix"
```

```
##    [[ suppressing 50 column names 'AAACATACAACCAC-1', 'AAACATTGATCAGC-1', 'AAACCGTGCTTCCG-1' ... ]]
```

```
##                                                                                
## CD3D  4 8 . . 1 2 3 2 . . 2 6 1 . . 2 . . . . . 4 3 3 1 4 . . 2 . 1 . . 5 . 2 2
## TCL1A . . . . . . . 1 . . . . . . . . 1 . . . . . . . . . 2 1 . . . . . . 1 . .
## MS4A1 . . . . . . . 1 2 1 . . . . . . 1 2 . . 2 . . . . . 4 . . . . . . . . . .
##                                
## CD3D  3 . 2 6 2 . 5 . . . 4 2 .
## TCL1A . . . . . . . . . . . . 4
## MS4A1 . . . . . . . . . . . . 4
```

## Comparisons for Each Cluster vs Rest of Cells

This is usually the first step to identify cell clusters. It will run the following comparison for each cluster c
- Group 1: All cells belonging to cluster c
- Group 2: All cells belonging to other clusters


```r
pbmc.markers <- FindAllMarkers(pbmc.filt, 
                               test.use = "wilcox",
                               assay = "SCT",
                               # slot = "scale.data",
                               # only.pos = TRUE, 
                               min.pct = 0.25,
                               logfc.threshold = 0.25,
                               verbose = T)
```

```
## Calculating cluster 0
```

```
## Calculating cluster 1
```

```
## Calculating cluster 2
```

```
## Calculating cluster 3
```

```
## Calculating cluster 4
```

```
## Calculating cluster 5
```

```
## Calculating cluster 6
```

```
## Calculating cluster 7
```

Examine top 2 genes in each cluster

```r
pbmc.markers %>%
    group_by(cluster) %>%
    slice_max(n = 2, order_by = avg_log2FC)
```

```
## # A tibble: 16 × 7
## # Groups:   cluster [8]
##        p_val avg_log2FC pct.1 pct.2 p_val_adj cluster gene   
##        <dbl>      <dbl> <dbl> <dbl>     <dbl> <fct>   <chr>  
##  1 8.00e-120      0.832 0.956 0.576 9.76e-116 0       LDHB   
##  2 6.99e-126      0.801 1     0.981 8.52e-122 0       RPS3A  
##  3 0              4.49  0.991 0.19  0         1       S100A9 
##  4 5.49e-259      4.36  0.998 0.508 6.69e-255 1       LYZ    
##  5 4.00e- 91      1.26  0.977 0.655 4.88e- 87 2       LTB    
##  6 2.40e- 91      1.03  0.95  0.474 2.92e- 87 2       IL32   
##  7 8.89e-184      2.81  1     0.829 1.08e-179 3       CD74   
##  8 1.08e-181      2.61  0.997 0.478 1.32e-177 3       HLA-DRA
##  9 1.85e-249      2.84  0.994 0.198 2.26e-245 4       CCL5   
## 10 1.69e-163      1.78  0.893 0.195 2.06e-159 4       NKG7   
## 11 5.43e-185      4.14  0.972 0.128 6.61e-181 5       GNLY   
## 12 1.20e-132      3.44  1     0.244 1.47e-128 5       NKG7   
## 13 5.52e- 91      2.71  1     0.302 6.73e- 87 6       LST1   
## 14 4.88e- 82      2.36  1     0.321 5.95e- 78 6       AIF1   
## 15 8.46e- 55      5.48  1     0.023 1.03e- 50 7       PPBP   
## 16 1.40e-111      4.31  1     0.01  1.71e-107 7       PF4
```

### Visualize Cluster DGE

Make a heatmap of top 2 genes for each cluster

```r
pbmc.markers %>%
    group_by(cluster) %>%
    slice_max(n = 2, order_by = avg_log2FC) -> pbmc.top.markers
DoHeatmap(pbmc.filt, features = pbmc.top.markers$gene) + NoLegend()
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-35-1.png)<!-- -->

### Overlay DGE by Gene on UMAP


```r
FeaturePlot(pbmc.filt, 
            features = pbmc.top.markers$gene, 
            label = T,
            ncol = 4) + 
  NoLegend()
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-36-1.png)<!-- -->

## Comparisons Between Specific Clusters

You can also compare two clusters specifically, for example to decide whether they are biologically different.

Cluster 1 vs cluster 2

```r
markers.0.2 <- FindMarkers(pbmc.filt, 
            ident.1 = "1", 
            ident.2 = "2", 
            test.use = "wilcox",
            assay = "SCT",
            logfc.threshold = 0.25,
            min.diff.pct = 0.25)

markers.0.2 %>%
    slice_max(n = 5, order_by = avg_log2FC)
```

```
##                p_val avg_log2FC pct.1 pct.2     p_val_adj
## S100A9 4.469568e-153   4.634443 0.991 0.184 5.449745e-149
## LYZ    1.220086e-148   4.540121 0.998 0.511 1.487651e-144
## S100A8 8.721035e-149   3.939970 0.962 0.098 1.063356e-144
## CST3   1.188536e-153   3.450618 0.991 0.198 1.449182e-149
## TYROBP 1.820093e-157   3.408568 0.993 0.132 2.219239e-153
```


## Annotation

If you have known markers, you can visualize their expression relative to the clusters


```r
FeaturePlot(pbmc.filt, 
            features = c("IL7R", "CCR7", "S100A4", "MS4A1", "CD8A", "FCGR3A", "MS4A7", "GNLY", "NKG7", "FCER1A", "CST3", "PPBP"), 
            label = T,
            ncol = 4) + 
NoLegend()
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-38-1.png)<!-- -->

### Set Cluster Names

Using the differentially expressed genes, marker expression, and domain knowledge, you can manually label the clusters.

|	Markers	      |	Cell Type     |
|---------------|:-------------:|
|	IL7R, CCR7	  |	Naive CD4+ T  |
|	IL7R, S100A4	|	Memory CD4+   |
|	MS4A1	        |	B             |
|	CD8A	        |	CD8+ T        |
|	FCGR3A, MS4A7	|	FCGR3A+ Mono  |
|	GNLY, NKG7	  |	NK            |
|	FCER1A, CST3	|	DC            |
|	PPBP	        |	Platelet      |



```r
new.cluster.ids <- c(
    "Naive CD4 T", 
    "Memory CD4 T",
    "CD14+ Mono", 
    "B", 
    "CD8 T",
    "NK", 
    "FCGR3A+ Mono",
    "Platelets"
    )
names(new.cluster.ids) <- levels(pbmc.filt)
pbmc.filt <- RenameIdents(pbmc.filt, new.cluster.ids)
```

### Re-Visualize


```r
DimPlot(pbmc.filt, reduction = "umap", label = TRUE, pt.size = 0.5) + NoLegend()
```

![](seurat-tutorial_files/figure-html/unnamed-chunk-40-1.png)<!-- -->

# Session Info


```r
sessionInfo()
```

```
## R version 4.2.0 (2022-04-22)
## Platform: x86_64-pc-linux-gnu (64-bit)
## Running under: Ubuntu 20.04.4 LTS
## 
## Matrix products: default
## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
## 
## locale:
##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
## 
## attached base packages:
## [1] stats     graphics  grDevices utils     datasets  methods   base     
## 
## other attached packages:
##  [1] fs_1.5.2           here_1.0.1         reticulate_1.25    patchwork_1.1.1   
##  [5] sp_1.4-7           SeuratObject_4.1.0 Seurat_4.1.1.9001  forcats_0.5.1     
##  [9] stringr_1.4.0      dplyr_1.0.9        purrr_0.3.4        readr_2.1.2       
## [13] tidyr_1.2.0        tibble_3.1.7       ggplot2_3.3.6      tidyverse_1.3.1   
## 
## loaded via a namespace (and not attached):
##   [1] readxl_1.4.0                backports_1.4.1            
##   [3] plyr_1.8.7                  igraph_1.3.1               
##   [5] lazyeval_0.2.2              splines_4.2.0              
##   [7] listenv_0.8.0               scattermore_0.8            
##   [9] GenomeInfoDb_1.32.2         digest_0.6.29              
##  [11] htmltools_0.5.2             fansi_1.0.3                
##  [13] magrittr_2.0.3              tensor_1.5                 
##  [15] cluster_2.1.3               ROCR_1.0-11                
##  [17] limma_3.52.1                tzdb_0.3.0                 
##  [19] globals_0.15.0              modelr_0.1.8               
##  [21] matrixStats_0.62.0          spatstat.sparse_2.1-1      
##  [23] colorspace_2.0-3            rvest_1.0.2                
##  [25] ggrepel_0.9.1               haven_2.5.0                
##  [27] xfun_0.31                   RCurl_1.98-1.6             
##  [29] crayon_1.5.1                jsonlite_1.8.0             
##  [31] progressr_0.10.1            spatstat.data_2.2-0        
##  [33] survival_3.2-13             zoo_1.8-10                 
##  [35] glue_1.6.2                  polyclip_1.10-0            
##  [37] gtable_0.3.0                zlibbioc_1.42.0            
##  [39] XVector_0.36.0              leiden_0.4.2               
##  [41] DelayedArray_0.22.0         future.apply_1.9.0         
##  [43] BiocGenerics_0.42.0         abind_1.4-5                
##  [45] scales_1.2.0                DBI_1.1.2                  
##  [47] spatstat.random_2.2-0       miniUI_0.1.1.1             
##  [49] Rcpp_1.0.8.3                viridisLite_0.4.0          
##  [51] xtable_1.8-4                spatstat.core_2.4-4        
##  [53] stats4_4.2.0                htmlwidgets_1.5.4          
##  [55] httr_1.4.3                  RColorBrewer_1.1-3         
##  [57] ellipsis_0.3.2              ica_1.0-2                  
##  [59] farver_2.1.0                pkgconfig_2.0.3            
##  [61] sass_0.4.1                  uwot_0.1.11                
##  [63] dbplyr_2.1.1                deldir_1.0-6               
##  [65] utf8_1.2.2                  labeling_0.4.2             
##  [67] tidyselect_1.1.2            rlang_1.0.2                
##  [69] reshape2_1.4.4              later_1.3.0                
##  [71] munsell_0.5.0               cellranger_1.1.0           
##  [73] tools_4.2.0                 cli_3.3.0                  
##  [75] generics_0.1.2              broom_0.8.0                
##  [77] ggridges_0.5.3              evaluate_0.15              
##  [79] fastmap_1.1.0               yaml_2.3.5                 
##  [81] goftest_1.2-3               knitr_1.39                 
##  [83] fitdistrplus_1.1-8          RANN_2.6.1                 
##  [85] sparseMatrixStats_1.8.0     pbapply_1.5-0              
##  [87] future_1.26.1               nlme_3.1-157               
##  [89] mime_0.12                   ggrastr_1.0.1              
##  [91] xml2_1.3.3                  compiler_4.2.0             
##  [93] rstudioapi_0.13             beeswarm_0.4.0             
##  [95] plotly_4.10.0               png_0.1-7                  
##  [97] spatstat.utils_2.3-1        reprex_2.0.1               
##  [99] glmGamPoi_1.8.0             bslib_0.3.1                
## [101] stringi_1.7.6               highr_0.9                  
## [103] RSpectra_0.16-1             rgeos_0.5-9                
## [105] lattice_0.20-45             Matrix_1.4-1               
## [107] vctrs_0.4.1                 pillar_1.7.0               
## [109] lifecycle_1.0.1             spatstat.geom_2.4-0        
## [111] lmtest_0.9-40               jquerylib_0.1.4            
## [113] RcppAnnoy_0.0.19            bitops_1.0-7               
## [115] data.table_1.14.2           cowplot_1.1.1              
## [117] irlba_2.3.5                 GenomicRanges_1.48.0       
## [119] httpuv_1.6.5                R6_2.5.1                   
## [121] promises_1.2.0.1            KernSmooth_2.23-20         
## [123] gridExtra_2.3               IRanges_2.30.0             
## [125] vipor_0.4.5                 parallelly_1.31.1          
## [127] codetools_0.2-18            MASS_7.3-57                
## [129] assertthat_0.2.1            SummarizedExperiment_1.26.1
## [131] rprojroot_2.0.3             withr_2.5.0                
## [133] sctransform_0.3.3           GenomeInfoDbData_1.2.8     
## [135] S4Vectors_0.34.0            mgcv_1.8-40                
## [137] parallel_4.2.0              hms_1.1.1                  
## [139] grid_4.2.0                  rpart_4.1.16               
## [141] DelayedMatrixStats_1.18.0   rmarkdown_2.14             
## [143] MatrixGenerics_1.8.0        Rtsne_0.16                 
## [145] Biobase_2.56.0              shiny_1.7.1                
## [147] lubridate_1.8.0             ggbeeswarm_0.6.0
```

